import 'dart:async';
import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

class AppLocalizations {
  AppLocalizations(this.locale, this.bundles);

  final Locale locale;
  final Map<String, String> bundles;

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  String text(String key) {
    String translation = bundles[key];
    if (translation == null) {
      return "!" + key + "!";
    } else {
      return translation;
    }
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'de'].contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale) async {
    Map<String, String> bundles = new HashMap();
    String fileContent = await rootBundle
        .loadString('bundles/bundle_' + locale.languageCode + ".properties");
    List<String> lines = fileContent.split("\n");
    lines
        .map((f) => f.trim())
        .where((f) => !f.startsWith("#") && f.length > 0)
        .forEach((bundle) {
      List<String> keyValue = bundle.split("=");
      assert(keyValue.length == 2);
      assert(!bundles.containsKey(keyValue[0]));
      bundles.putIfAbsent(keyValue[0], () => keyValue[1]);
    });

    return SynchronousFuture<AppLocalizations>(
        AppLocalizations(locale, bundles));
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) {
    bool shouldReload = false;
    assert(shouldReload = true);
    return shouldReload;
  }
}
